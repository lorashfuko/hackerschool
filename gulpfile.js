const gulp        = require('gulp'),
      sass        = require('gulp-sass'),
      sourcemaps  = require('gulp-sourcemaps'),
      // concat      = require('gulp-concat'),
      // uglify      = require('gulp-uglify'),
      browserSync = require('browser-sync');

gulp.task('html', function () {
  browserSync.init({
    server: {
      baseDir: "./www"
    }
  });
});

gulp.task('sass', function () {
  return gulp.src('./build/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./www/css'))
    // .pipe(browserSync.stream({stream: true}));
});

gulp.task('js', function () {
  return gulp.src('./build/js/**/*.js')
    .pipe(sourcemaps.init())
    // .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./www/scripts'))
    // .pipe(browserSync.stream({stream: true}));
});

gulp.task('watch', function () {
  gulp.watch('./build/scss/**/*.scss', gulp.series('sass'));
  gulp.watch('./build/js/**/*.js', gulp.series('js'));
  // gulp.watch('./www/*.html', browserSync.reload);
});

gulp.task('default', gulp.series('watch'));


