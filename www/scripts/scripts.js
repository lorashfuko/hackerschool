"use strict";
document.addEventListener("DOMContentLoaded", function () {
  //Выбираем ссылки на котрые будем кликать
  let linksMenu = document.querySelectorAll(".menu__item a[href*='#']");

  for (let link of linksMenu) {
    link.addEventListener("click", function (e) {
      e.preventDefault();//Отменяем стандартное поведение скрола до якоря

      const linkId = link.getAttribute("href");//#something

      document.querySelector('' + linkId).scrollIntoView({
        behavior: "smooth",
        block: "start"
      });
    })
  }

});

//# sourceMappingURL=scripts.js.map
